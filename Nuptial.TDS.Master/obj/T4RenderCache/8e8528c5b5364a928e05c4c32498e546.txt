//Revision: 55270a2a-baef-48d3-8418-4f6a9c83a35d



namespace NXA.Repository.Models.Base
{
	
	/// <summary>
	/// BaseParametersTemplate
	/// <para></para>
	/// <para>Path: /sitecore/templates/NXA/Base/_BaseParametersTemplate</para>	
	/// <para>ID: 8e8528c5-b536-4a92-8e05-c4c32498e546</para>	
	/// </summary>
    [SitecoreType(TemplateId = "8e8528c5-b536-4a92-8e05-c4c32498e546", AutoMap = true)]
	public partial class BaseParametersTemplate : BaseModel
	{
		public static ID TemplateId = new ID("8e8528c5-b536-4a92-8e05-c4c32498e546"); 

	    public struct FieldNames {
	    }
	    public struct FieldIds {
	    }

	
	}
}

namespace NXA.Repository.Interfaces.Base
{	
    public partial interface IBaseParametersTemplateRepository
    {
        NXA.Repository.Models.Base.BaseParametersTemplate GetBaseParametersTemplate(Item item);
        NXA.Repository.Models.Base.BaseParametersTemplate GetBaseParametersTemplate(ID itemId);
        NXA.Repository.Models.Base.BaseParametersTemplate GetBaseParametersTemplate(string path);
        IEnumerable<NXA.Repository.Models.Base.BaseParametersTemplate> GetBaseParametersTemplatesFromSite(SiteContext site = null);
        NXA.Repository.Models.Base.BaseParametersTemplate GetBaseParametersTemplateFromSite(SiteContext site = null, string itemPath = "");
    }
}

namespace NXA.Repository.Repositories.Base
{
    public partial class BaseParametersTemplateRepository : BaseRepository, NXA.Repository.Interfaces.Base.IBaseParametersTemplateRepository
    {
        public NXA.Repository.Models.Base.BaseParametersTemplate GetBaseParametersTemplate(Item item)
		{
			return SitecoreContext.Cast<NXA.Repository.Models.Base.BaseParametersTemplate>(item);
		}

        public NXA.Repository.Models.Base.BaseParametersTemplate GetBaseParametersTemplate(ID itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Base.BaseParametersTemplate>(itemId.Guid);
		}

        public NXA.Repository.Models.Base.BaseParametersTemplate GetBaseParametersTemplate(string path)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Base.BaseParametersTemplate>(path);		
		}

        public IEnumerable<NXA.Repository.Models.Base.BaseParametersTemplate> GetBaseParametersTemplatesFromSite(SiteContext site = null)
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			// Format path to safe quering
            var contentStartPath = site.ContentStartPath.GetFormattedPath();

			// Execute query and get all possible options
			var query =
				$"fast:{contentStartPath}//*[@@templateid='{NXA.Repository.Models.Base.BaseParametersTemplate.TemplateId}']";
			var allItems = Sitecore.Context.ContentDatabase.SelectItems(query).Select(GetBaseParametersTemplate).ToList();
			return allItems;
		}

        public NXA.Repository.Models.Base.BaseParametersTemplate GetBaseParametersTemplateFromSite(SiteContext site = null, string itemPath = "")
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			if (!string.IsNullOrEmpty(itemPath)) {
				var contentStartPath = site.ContentStartPath + itemPath;
				var path = $"/{contentStartPath}{itemPath}".GetFormattedPath();
				return GetBaseParametersTemplate(path);
			}

			var allItems = GetBaseParametersTemplatesFromSite(site);
			return allItems.Any() ? allItems.First() : null;
		}
    }
}


