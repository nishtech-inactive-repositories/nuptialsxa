//Revision: bc3bcdda-00da-49a3-99b5-312c6239d8d0



namespace NXA.Repository.Models.Data.Scripts
{
	
	/// <summary>
	/// ScriptsFolder
	/// <para></para>
	/// <para>Path: /sitecore/templates/NXA/Data/Scripts/Scripts Folder</para>	
	/// <para>ID: 38eebc8c-9f84-45bd-9281-2ab653eb2e94</para>	
	/// </summary>
    [SitecoreType(TemplateId = "38eebc8c-9f84-45bd-9281-2ab653eb2e94", AutoMap = true)]
	public partial class ScriptsFolder : BaseModel
	{
		public static ID TemplateId = new ID("38eebc8c-9f84-45bd-9281-2ab653eb2e94"); 

	    public struct FieldNames {
	    }
	    public struct FieldIds {
	    }
	    public struct FieldSources {
	    }

	

	}
}

namespace NXA.Repository.Interfaces.Data.Scripts
{	
    public partial interface IScriptsFolderRepository
    {
        NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolder(Item item);
        NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolder(Guid itemId);
        NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolder(ID itemId);
        NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolder(string path);
        IEnumerable<NXA.Repository.Models.Data.Scripts.ScriptsFolder> GetScriptsFoldersFromSite(SiteContext site = null);
		IEnumerable<NXA.Repository.Models.Data.Scripts.ScriptsFolder> GetScriptsFoldersFromSite(Item contextItem);
        NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolderFromSite(SiteContext site = null, string itemPath = "");
        NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolderFromSite(Item contextItem, string itemPath = "");
		NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolderFromGlobalSettings();
    }
}

namespace NXA.Repository.Repositories.Data.Scripts
{
    public partial class ScriptsFolderRepository : BaseRepository, NXA.Repository.Interfaces.Data.Scripts.IScriptsFolderRepository
    {
        public NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolder(Item item)
		{
			return SitecoreContext.Cast<NXA.Repository.Models.Data.Scripts.ScriptsFolder>(item);
		}

        public NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolder(Guid itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.Scripts.ScriptsFolder>(itemId);
		}

        public NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolder(ID itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.Scripts.ScriptsFolder>(itemId.Guid);
		}

        public NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolder(string path)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.Scripts.ScriptsFolder>(path);		
		}

        public IEnumerable<NXA.Repository.Models.Data.Scripts.ScriptsFolder> GetScriptsFoldersFromSite(SiteContext site = null)
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			// Format path to safe quering
            var contentStartPath = site.ContentStartPath.GetFormattedPath();

			// Execute query and get all possible options
			var query =
				$"fast:{contentStartPath}//*[@@templateid='{NXA.Repository.Models.Data.Scripts.ScriptsFolder.TemplateId}']";
			var allItems = Sitecore.Context.Database.SelectItems(query).Select(GetScriptsFolder).ToList();
			return allItems;
		}

        public IEnumerable<NXA.Repository.Models.Data.Scripts.ScriptsFolder> GetScriptsFoldersFromSite(Item contextItem)
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetScriptsFoldersFromSite(site);
        }

        public NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolderFromSite(SiteContext site = null, string itemPath = "")
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			if (!string.IsNullOrEmpty(itemPath)) {
				var contentStartPath = site.ContentStartPath + itemPath;
				var path = $"/{contentStartPath}{itemPath}".GetFormattedPath();
				return GetScriptsFolder(path);
			}

			var allItems = GetScriptsFoldersFromSite(site);
			return allItems.Any() ? allItems.First() : null;
		}

        public NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolderFromSite(Item contextItem, string itemPath = "")
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetScriptsFolderFromSite(site, itemPath);
        }

        private readonly NXA.Repository.Multisite.Providers.IGlobalSettingsProvider _settingsProvider =
            DependencyResolver.Current.GetService<NXA.Repository.Multisite.Providers.IGlobalSettingsProvider>();
        private Item _globalScriptsFolder;
        public NXA.Repository.Models.Data.Scripts.ScriptsFolder GetScriptsFolderFromGlobalSettings()
		{
            if (_globalScriptsFolder != null)
                return GetScriptsFolder(_globalScriptsFolder);

            var settingsRoot = _settingsProvider.GetSettingsRoot();
            var item = settingsRoot.Axes.GetDescendants().FirstOrDefault(p =>
                p.TemplateID == NXA.Repository.Models.Data.Scripts.ScriptsFolder.TemplateId);
			_globalScriptsFolder = item;
            return item==null ? null : GetScriptsFolder(item);
		}
    }
}


