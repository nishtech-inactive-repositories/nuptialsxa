//Revision: a0ed5c20-2cf1-48be-868a-e483d5e46847



namespace NXA.Repository.Models.Data.Redirect
{
	
	/// <summary>
	/// ResponseStatusCode
	/// <para></para>
	/// <para>Path: /sitecore/templates/NXA/Data/Redirect/Response Status Code</para>	
	/// <para>ID: 14a09c18-b21a-4789-b3b7-307e4c006f60</para>	
	/// </summary>
    [SitecoreType(TemplateId = "14a09c18-b21a-4789-b3b7-307e4c006f60", AutoMap = true)]
	public partial class ResponseStatusCode : BaseModel
	{
		public static ID TemplateId = new ID("14a09c18-b21a-4789-b3b7-307e4c006f60"); 

	    public struct FieldNames {
	        public const string StatusCode = "Status Code";
	    }
	    public struct FieldIds {
	        public static readonly ID StatusCode = new ID("95e3e57d-d0e2-4f62-88c2-c5f1b7c31f63");
	    }
	    public struct FieldSources {
	    }

		/// <summary>
		/// The Status Code field.
		/// <para></para>
		/// <para>Field Type: Integer</para>		
		/// <para>Field ID: 95e3e57d-d0e2-4f62-88c2-c5f1b7c31f63</para>
		/// <para>Custom Data: </para>
		/// </summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("TDS - GlassItem.tt", "1.0")]
		[SitecoreField("Status Code")]
		public virtual int StatusCode  { get; set; }
	

	}
}

namespace NXA.Repository.Interfaces.Data.Redirect
{	
    public partial interface IResponseStatusCodeRepository
    {
        NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCode(Item item);
        NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCode(Guid itemId);
        NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCode(ID itemId);
        NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCode(string path);
        IEnumerable<NXA.Repository.Models.Data.Redirect.ResponseStatusCode> GetResponseStatusCodesFromSite(SiteContext site = null);
		IEnumerable<NXA.Repository.Models.Data.Redirect.ResponseStatusCode> GetResponseStatusCodesFromSite(Item contextItem);
        NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCodeFromSite(SiteContext site = null, string itemPath = "");
        NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCodeFromSite(Item contextItem, string itemPath = "");
		NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCodeFromGlobalSettings();
    }
}

namespace NXA.Repository.Repositories.Data.Redirect
{
    public partial class ResponseStatusCodeRepository : BaseRepository, NXA.Repository.Interfaces.Data.Redirect.IResponseStatusCodeRepository
    {
        public NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCode(Item item)
		{
			return SitecoreContext.Cast<NXA.Repository.Models.Data.Redirect.ResponseStatusCode>(item);
		}

        public NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCode(Guid itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.Redirect.ResponseStatusCode>(itemId);
		}

        public NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCode(ID itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.Redirect.ResponseStatusCode>(itemId.Guid);
		}

        public NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCode(string path)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.Redirect.ResponseStatusCode>(path);		
		}

        public IEnumerable<NXA.Repository.Models.Data.Redirect.ResponseStatusCode> GetResponseStatusCodesFromSite(SiteContext site = null)
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			// Format path to safe quering
            var contentStartPath = site.ContentStartPath.GetFormattedPath();

			// Execute query and get all possible options
			var query =
				$"fast:{contentStartPath}//*[@@templateid='{NXA.Repository.Models.Data.Redirect.ResponseStatusCode.TemplateId}']";
			var allItems = Sitecore.Context.Database.SelectItems(query).Select(GetResponseStatusCode).ToList();
			return allItems;
		}

        public IEnumerable<NXA.Repository.Models.Data.Redirect.ResponseStatusCode> GetResponseStatusCodesFromSite(Item contextItem)
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetResponseStatusCodesFromSite(site);
        }

        public NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCodeFromSite(SiteContext site = null, string itemPath = "")
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			if (!string.IsNullOrEmpty(itemPath)) {
				var contentStartPath = site.ContentStartPath + itemPath;
				var path = $"/{contentStartPath}{itemPath}".GetFormattedPath();
				return GetResponseStatusCode(path);
			}

			var allItems = GetResponseStatusCodesFromSite(site);
			return allItems.Any() ? allItems.First() : null;
		}

        public NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCodeFromSite(Item contextItem, string itemPath = "")
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetResponseStatusCodeFromSite(site, itemPath);
        }

        private readonly NXA.Repository.Multisite.Providers.IGlobalSettingsProvider _settingsProvider =
            DependencyResolver.Current.GetService<NXA.Repository.Multisite.Providers.IGlobalSettingsProvider>();
        private Item _globalResponseStatusCode;
        public NXA.Repository.Models.Data.Redirect.ResponseStatusCode GetResponseStatusCodeFromGlobalSettings()
		{
            if (_globalResponseStatusCode != null)
                return GetResponseStatusCode(_globalResponseStatusCode);

            var settingsRoot = _settingsProvider.GetSettingsRoot();
            var item = settingsRoot.Axes.GetDescendants().FirstOrDefault(p =>
                p.TemplateID == NXA.Repository.Models.Data.Redirect.ResponseStatusCode.TemplateId);
			_globalResponseStatusCode = item;
            return item==null ? null : GetResponseStatusCode(item);
		}
    }
}


