//Revision: b011789b-e3a3-4ec5-a5e9-241006721bb4



namespace NXA.Repository.Models.SiteStructure
{
	
	/// <summary>
	/// Website
	/// <para></para>
	/// <para>Path: /sitecore/templates/NXA/Site Structure/Website</para>	
	/// <para>ID: 7d951b31-cc0b-4c97-a259-ad75d9991e9b</para>	
	/// </summary>
    [SitecoreType(TemplateId = "7d951b31-cc0b-4c97-a259-ad75d9991e9b", AutoMap = true)]
	public partial class Website : BaseModel
	{
		public static ID TemplateId = new ID("7d951b31-cc0b-4c97-a259-ad75d9991e9b"); 

	    public struct FieldNames {
	    }
	    public struct FieldIds {
	    }
	    public struct FieldSources {
	    }

	

	}
}

namespace NXA.Repository.Interfaces.SiteStructure
{	
    public partial interface IWebsiteRepository
    {
        NXA.Repository.Models.SiteStructure.Website GetWebsite(Item item);
        NXA.Repository.Models.SiteStructure.Website GetWebsite(Guid itemId);
        NXA.Repository.Models.SiteStructure.Website GetWebsite(ID itemId);
        NXA.Repository.Models.SiteStructure.Website GetWebsite(string path);
        IEnumerable<NXA.Repository.Models.SiteStructure.Website> GetWebsitesFromSite(SiteContext site = null);
		IEnumerable<NXA.Repository.Models.SiteStructure.Website> GetWebsitesFromSite(Item contextItem);
        NXA.Repository.Models.SiteStructure.Website GetWebsiteFromSite(SiteContext site = null, string itemPath = "");
        NXA.Repository.Models.SiteStructure.Website GetWebsiteFromSite(Item contextItem, string itemPath = "");
		NXA.Repository.Models.SiteStructure.Website GetWebsiteFromGlobalSettings();
    }
}

namespace NXA.Repository.Repositories.SiteStructure
{
    public partial class WebsiteRepository : BaseRepository, NXA.Repository.Interfaces.SiteStructure.IWebsiteRepository
    {
        public NXA.Repository.Models.SiteStructure.Website GetWebsite(Item item)
		{
			return SitecoreContext.Cast<NXA.Repository.Models.SiteStructure.Website>(item);
		}

        public NXA.Repository.Models.SiteStructure.Website GetWebsite(Guid itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.SiteStructure.Website>(itemId);
		}

        public NXA.Repository.Models.SiteStructure.Website GetWebsite(ID itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.SiteStructure.Website>(itemId.Guid);
		}

        public NXA.Repository.Models.SiteStructure.Website GetWebsite(string path)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.SiteStructure.Website>(path);		
		}

        public IEnumerable<NXA.Repository.Models.SiteStructure.Website> GetWebsitesFromSite(SiteContext site = null)
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			// Format path to safe quering
            var contentStartPath = site.ContentStartPath.GetFormattedPath();

			// Execute query and get all possible options
			var query =
				$"fast:{contentStartPath}//*[@@templateid='{NXA.Repository.Models.SiteStructure.Website.TemplateId}']";
			var allItems = Sitecore.Context.Database.SelectItems(query).Select(GetWebsite).ToList();
			return allItems;
		}

        public IEnumerable<NXA.Repository.Models.SiteStructure.Website> GetWebsitesFromSite(Item contextItem)
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetWebsitesFromSite(site);
        }

        public NXA.Repository.Models.SiteStructure.Website GetWebsiteFromSite(SiteContext site = null, string itemPath = "")
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			if (!string.IsNullOrEmpty(itemPath)) {
				var contentStartPath = site.ContentStartPath + itemPath;
				var path = $"/{contentStartPath}{itemPath}".GetFormattedPath();
				return GetWebsite(path);
			}

			var allItems = GetWebsitesFromSite(site);
			return allItems.Any() ? allItems.First() : null;
		}

        public NXA.Repository.Models.SiteStructure.Website GetWebsiteFromSite(Item contextItem, string itemPath = "")
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetWebsiteFromSite(site, itemPath);
        }

        private readonly NXA.Repository.Multisite.Providers.IGlobalSettingsProvider _settingsProvider =
            DependencyResolver.Current.GetService<NXA.Repository.Multisite.Providers.IGlobalSettingsProvider>();
        private Item _globalWebsite;
        public NXA.Repository.Models.SiteStructure.Website GetWebsiteFromGlobalSettings()
		{
            if (_globalWebsite != null)
                return GetWebsite(_globalWebsite);

            var settingsRoot = _settingsProvider.GetSettingsRoot();
            var item = settingsRoot.Axes.GetDescendants().FirstOrDefault(p =>
                p.TemplateID == NXA.Repository.Models.SiteStructure.Website.TemplateId);
			_globalWebsite = item;
            return item==null ? null : GetWebsite(item);
		}
    }
}


