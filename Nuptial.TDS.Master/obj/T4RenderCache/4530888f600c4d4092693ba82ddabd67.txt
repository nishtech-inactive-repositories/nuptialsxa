//Revision: d4e17dfd-72b4-4049-8fd8-58def51b2f46



namespace NXA.Repository.Models.Data.Forms.SendMail
{
	
	/// <summary>
	/// EMailTemplatesFolder
	/// <para></para>
	/// <para>Path: /sitecore/templates/NXA/Data/Forms/Send Mail/E-mail Templates Folder</para>	
	/// <para>ID: 4530888f-600c-4d40-9269-3ba82ddabd67</para>	
	/// </summary>
    [SitecoreType(TemplateId = "4530888f-600c-4d40-9269-3ba82ddabd67", AutoMap = true)]
	public partial class EMailTemplatesFolder : BaseModel
	{
		public static ID TemplateId = new ID("4530888f-600c-4d40-9269-3ba82ddabd67"); 

	    public struct FieldNames {
	    }
	    public struct FieldIds {
	    }
	    public struct FieldSources {
	    }

	

	}
}

namespace NXA.Repository.Interfaces.Data.Forms.SendMail
{	
    public partial interface IEMailTemplatesFolderRepository
    {
        NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolder(Item item);
        NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolder(Guid itemId);
        NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolder(ID itemId);
        NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolder(string path);
        IEnumerable<NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder> GetEMailTemplatesFoldersFromSite(SiteContext site = null);
		IEnumerable<NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder> GetEMailTemplatesFoldersFromSite(Item contextItem);
        NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolderFromSite(SiteContext site = null, string itemPath = "");
        NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolderFromSite(Item contextItem, string itemPath = "");
		NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolderFromGlobalSettings();
    }
}

namespace NXA.Repository.Repositories.Data.Forms.SendMail
{
    public partial class EMailTemplatesFolderRepository : BaseRepository, NXA.Repository.Interfaces.Data.Forms.SendMail.IEMailTemplatesFolderRepository
    {
        public NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolder(Item item)
		{
			return SitecoreContext.Cast<NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder>(item);
		}

        public NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolder(Guid itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder>(itemId);
		}

        public NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolder(ID itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder>(itemId.Guid);
		}

        public NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolder(string path)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder>(path);		
		}

        public IEnumerable<NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder> GetEMailTemplatesFoldersFromSite(SiteContext site = null)
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			// Format path to safe quering
            var contentStartPath = site.ContentStartPath.GetFormattedPath();

			// Execute query and get all possible options
			var query =
				$"fast:{contentStartPath}//*[@@templateid='{NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder.TemplateId}']";
			var allItems = Sitecore.Context.Database.SelectItems(query).Select(GetEMailTemplatesFolder).ToList();
			return allItems;
		}

        public IEnumerable<NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder> GetEMailTemplatesFoldersFromSite(Item contextItem)
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetEMailTemplatesFoldersFromSite(site);
        }

        public NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolderFromSite(SiteContext site = null, string itemPath = "")
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			if (!string.IsNullOrEmpty(itemPath)) {
				var contentStartPath = site.ContentStartPath + itemPath;
				var path = $"/{contentStartPath}{itemPath}".GetFormattedPath();
				return GetEMailTemplatesFolder(path);
			}

			var allItems = GetEMailTemplatesFoldersFromSite(site);
			return allItems.Any() ? allItems.First() : null;
		}

        public NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolderFromSite(Item contextItem, string itemPath = "")
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetEMailTemplatesFolderFromSite(site, itemPath);
        }

        private readonly NXA.Repository.Multisite.Providers.IGlobalSettingsProvider _settingsProvider =
            DependencyResolver.Current.GetService<NXA.Repository.Multisite.Providers.IGlobalSettingsProvider>();
        private Item _globalEMailTemplatesFolder;
        public NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder GetEMailTemplatesFolderFromGlobalSettings()
		{
            if (_globalEMailTemplatesFolder != null)
                return GetEMailTemplatesFolder(_globalEMailTemplatesFolder);

            var settingsRoot = _settingsProvider.GetSettingsRoot();
            var item = settingsRoot.Axes.GetDescendants().FirstOrDefault(p =>
                p.TemplateID == NXA.Repository.Models.Data.Forms.SendMail.EMailTemplatesFolder.TemplateId);
			_globalEMailTemplatesFolder = item;
            return item==null ? null : GetEMailTemplatesFolder(item);
		}
    }
}


