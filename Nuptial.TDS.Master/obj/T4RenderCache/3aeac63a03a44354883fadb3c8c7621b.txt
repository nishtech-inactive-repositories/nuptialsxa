//Revision: db005577-86d4-4252-8091-a38e403af34b



namespace NXA.Repository.Models.Components.Containers.Grids.Parameters
{
	
	/// <summary>
	/// ThreeEqualColumnsParameters
	/// <para></para>
	/// <para>Path: /sitecore/templates/NXA/Components/Containers/Grids/Parameters/_Three Equal Columns Parameters</para>	
	/// <para>ID: 3aeac63a-03a4-4354-883f-adb3c8c7621b</para>	
	/// </summary>
    [SitecoreType(TemplateId = "3aeac63a-03a4-4354-883f-adb3c8c7621b", AutoMap = true)]
	public partial class ThreeEqualColumnsParameters : BaseModel
	{
		public static ID TemplateId = new ID("3aeac63a-03a4-4354-883f-adb3c8c7621b"); 

	    public struct FieldNames {
	        public const string ColumnAttributes = "ColumnAttributes";
	        public const string ColumnCssClasses = "ColumnCssClasses";
	        public const string ColumnTag = "ColumnTag";
	        public const string HideRow = "HideRow";
	        public const string RowAttributes = "RowAttributes";
	        public const string RowCssClasses = "RowCssClasses";
	        public const string RowTag = "RowTag";
	    }
	    public struct FieldIds {
	        public static readonly ID ColumnAttributes = new ID("26e82e13-7489-4ba4-b6ed-4a476f121474");
	        public static readonly ID ColumnCssClasses = new ID("0ce9031b-ff0b-451c-8879-d4ac55d5574e");
	        public static readonly ID ColumnTag = new ID("d0ab5d00-3eba-446a-bae9-7d264ecdcc98");
	        public static readonly ID HideRow = new ID("0a086007-f76d-4475-a4c3-d9994c1959d6");
	        public static readonly ID RowAttributes = new ID("8606ded3-a8e4-4473-ab86-4e453936db07");
	        public static readonly ID RowCssClasses = new ID("95757897-d614-4e16-8e43-7e9dc6dcfc1b");
	        public static readonly ID RowTag = new ID("d86f232e-7d97-46b4-87c5-d411891fc2ef");
	    }
	    public struct FieldSources {
		    public static readonly string ColumnCssClasses = "site:Multicolumn - Column Classes";		
		    public static readonly string ColumnTag = "site:Column Tags";		
		    public static readonly string RowCssClasses = "site:Multicolumn - Row Classes";		
		    public static readonly string RowTag = "site:Row Tags";		
	    }

		/// <summary>
		/// The ColumnAttributes field.
		/// <para>These attributes will be injected at the column</para>
		/// <para>Field Type: Parameters Name Value List</para>		
		/// <para>Field ID: 26e82e13-7489-4ba4-b6ed-4a476f121474</para>
		/// <para>Custom Data: </para>
		/// </summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("TDS - GlassItem.tt", "1.0")]
		[SitecoreField("ColumnAttributes")]
		public virtual string ColumnAttributes  { get; set; }
		/// <summary>
		/// The ColumnCssClasses field.
		/// <para>Classes will be applied to all columns</para>
		/// <para>Field Type: Multilist</para>		
		/// <para>Field ID: 0ce9031b-ff0b-451c-8879-d4ac55d5574e</para>
		/// <para>Custom Data: </para>
		/// </summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("TDS - GlassItem.tt", "1.0")]
		[SitecoreField("ColumnCssClasses")]
		public virtual IEnumerable<Guid> ColumnCssClasses  { get; set; }
		/// <summary>
		/// The ColumnTag field.
		/// <para></para>
		/// <para>Field Type: Droplink</para>		
		/// <para>Field ID: d0ab5d00-3eba-446a-bae9-7d264ecdcc98</para>
		/// <para>Custom Data: </para>
		/// </summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("TDS - GlassItem.tt", "1.0")]
		[SitecoreField("ColumnTag")]
		public virtual Guid ColumnTag  { get; set; }
		/// <summary>
		/// The HideRow field.
		/// <para>If checked, will not inject the row div</para>
		/// <para>Field Type: Checkbox</para>		
		/// <para>Field ID: 0a086007-f76d-4475-a4c3-d9994c1959d6</para>
		/// <para>Custom Data: </para>
		/// </summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("TDS - GlassItem.tt", "1.0")]
		[SitecoreField("HideRow")]
		public virtual bool HideRow  { get; set; }
		/// <summary>
		/// The RowAttributes field.
		/// <para>These attributes will be injected at the row</para>
		/// <para>Field Type: Parameters Name Value List</para>		
		/// <para>Field ID: 8606ded3-a8e4-4473-ab86-4e453936db07</para>
		/// <para>Custom Data: </para>
		/// </summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("TDS - GlassItem.tt", "1.0")]
		[SitecoreField("RowAttributes")]
		public virtual string RowAttributes  { get; set; }
		/// <summary>
		/// The RowCssClasses field.
		/// <para>Only applicable if Hide Row is not checked</para>
		/// <para>Field Type: Multilist</para>		
		/// <para>Field ID: 95757897-d614-4e16-8e43-7e9dc6dcfc1b</para>
		/// <para>Custom Data: </para>
		/// </summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("TDS - GlassItem.tt", "1.0")]
		[SitecoreField("RowCssClasses")]
		public virtual IEnumerable<Guid> RowCssClasses  { get; set; }
		/// <summary>
		/// The RowTag field.
		/// <para></para>
		/// <para>Field Type: Droplink</para>		
		/// <para>Field ID: d86f232e-7d97-46b4-87c5-d411891fc2ef</para>
		/// <para>Custom Data: </para>
		/// </summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("TDS - GlassItem.tt", "1.0")]
		[SitecoreField("RowTag")]
		public virtual Guid RowTag  { get; set; }
	

	}
}

namespace NXA.Repository.Interfaces.Components.Containers.Grids.Parameters
{	
    public partial interface IThreeEqualColumnsParametersRepository
    {
        NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParameters(Item item);
        NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParameters(Guid itemId);
        NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParameters(ID itemId);
        NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParameters(string path);
        IEnumerable<NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters> GetThreeEqualColumnsParameterssFromSite(SiteContext site = null);
		IEnumerable<NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters> GetThreeEqualColumnsParameterssFromSite(Item contextItem);
        NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParametersFromSite(SiteContext site = null, string itemPath = "");
        NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParametersFromSite(Item contextItem, string itemPath = "");
		NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParametersFromGlobalSettings();
    }
}

namespace NXA.Repository.Repositories.Components.Containers.Grids.Parameters
{
    public partial class ThreeEqualColumnsParametersRepository : BaseRepository, NXA.Repository.Interfaces.Components.Containers.Grids.Parameters.IThreeEqualColumnsParametersRepository
    {
        public NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParameters(Item item)
		{
			return SitecoreContext.Cast<NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters>(item);
		}

        public NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParameters(Guid itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters>(itemId);
		}

        public NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParameters(ID itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters>(itemId.Guid);
		}

        public NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParameters(string path)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters>(path);		
		}

        public IEnumerable<NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters> GetThreeEqualColumnsParameterssFromSite(SiteContext site = null)
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			// Format path to safe quering
            var contentStartPath = site.ContentStartPath.GetFormattedPath();

			// Execute query and get all possible options
			var query =
				$"fast:{contentStartPath}//*[@@templateid='{NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters.TemplateId}']";
			var allItems = Sitecore.Context.Database.SelectItems(query).Select(GetThreeEqualColumnsParameters).ToList();
			return allItems;
		}

        public IEnumerable<NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters> GetThreeEqualColumnsParameterssFromSite(Item contextItem)
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetThreeEqualColumnsParameterssFromSite(site);
        }

        public NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParametersFromSite(SiteContext site = null, string itemPath = "")
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			if (!string.IsNullOrEmpty(itemPath)) {
				var contentStartPath = site.ContentStartPath + itemPath;
				var path = $"/{contentStartPath}{itemPath}".GetFormattedPath();
				return GetThreeEqualColumnsParameters(path);
			}

			var allItems = GetThreeEqualColumnsParameterssFromSite(site);
			return allItems.Any() ? allItems.First() : null;
		}

        public NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParametersFromSite(Item contextItem, string itemPath = "")
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetThreeEqualColumnsParametersFromSite(site, itemPath);
        }

        private readonly NXA.Repository.Multisite.Providers.IGlobalSettingsProvider _settingsProvider =
            DependencyResolver.Current.GetService<NXA.Repository.Multisite.Providers.IGlobalSettingsProvider>();
        private Item _globalThreeEqualColumnsParameters;
        public NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters GetThreeEqualColumnsParametersFromGlobalSettings()
		{
            if (_globalThreeEqualColumnsParameters != null)
                return GetThreeEqualColumnsParameters(_globalThreeEqualColumnsParameters);

            var settingsRoot = _settingsProvider.GetSettingsRoot();
            var item = settingsRoot.Axes.GetDescendants().FirstOrDefault(p =>
                p.TemplateID == NXA.Repository.Models.Components.Containers.Grids.Parameters.ThreeEqualColumnsParameters.TemplateId);
			_globalThreeEqualColumnsParameters = item;
            return item==null ? null : GetThreeEqualColumnsParameters(item);
		}
    }
}


