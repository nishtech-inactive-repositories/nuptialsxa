//Revision: ea8a1742-7ae3-4a3e-b645-cee1073ded06



namespace NXA.Repository.Models.Base
{
	
	/// <summary>
	/// BaseAddress
	/// <para></para>
	/// <para>Path: /sitecore/templates/NXA/Base/_Base Address</para>	
	/// <para>ID: 3b42e420-49b8-444e-9f69-f3f889f87c47</para>	
	/// </summary>
    [SitecoreType(TemplateId = "3b42e420-49b8-444e-9f69-f3f889f87c47", AutoMap = true)]
	public partial class BaseAddress : BaseModel
	{
		public static ID TemplateId = new ID("3b42e420-49b8-444e-9f69-f3f889f87c47"); 

	    public struct FieldNames {
	        public const string Addresses = "Addresses";
	        public const string InheritParentAddress = "Inherit Parent Address";
	    }
	    public struct FieldIds {
	        public static readonly ID Addresses = new ID("cc14d1fd-1b8a-46ec-86ea-546b6467080d");
	        public static readonly ID InheritParentAddress = new ID("634f51a4-7d52-40ed-b776-8dc76bda345f");
	    }

		/// <summary>
		/// The Addresses field.
		/// <para></para>
		/// <para>Field Type: Multilist</para>		
		/// <para>Field ID: cc14d1fd-1b8a-46ec-86ea-546b6467080d</para>
		/// <para>Custom Data: </para>
		/// </summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("TDS - GlassItem.tt", "1.0")]
		[SitecoreField("Addresses")]
		public virtual IEnumerable<Guid> Addresses  { get; set; }
		/// <summary>
		/// The Inherit Parent Address field.
		/// <para>if checked includes the parent address along with current address on the item</para>
		/// <para>Field Type: Checkbox</para>		
		/// <para>Field ID: 634f51a4-7d52-40ed-b776-8dc76bda345f</para>
		/// <para>Custom Data: </para>
		/// </summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("TDS - GlassItem.tt", "1.0")]
		[SitecoreField("Inherit Parent Address")]
		public virtual bool InheritParentAddress  { get; set; }
	
	}
}

namespace NXA.Repository.Interfaces.Base
{	
    public partial interface IBaseAddressRepository
    {
        NXA.Repository.Models.Base.BaseAddress GetBaseAddress(Item item);
        NXA.Repository.Models.Base.BaseAddress GetBaseAddress(ID itemId);
        NXA.Repository.Models.Base.BaseAddress GetBaseAddress(string path);
        IEnumerable<NXA.Repository.Models.Base.BaseAddress> GetBaseAddresssFromSite(SiteContext site = null);
        NXA.Repository.Models.Base.BaseAddress GetBaseAddressFromSite(SiteContext site = null, string itemPath = "");
    }
}

namespace NXA.Repository.Repositories.Base
{
    public partial class BaseAddressRepository : BaseRepository, NXA.Repository.Interfaces.Base.IBaseAddressRepository
    {
        public NXA.Repository.Models.Base.BaseAddress GetBaseAddress(Item item)
		{
			return SitecoreContext.Cast<NXA.Repository.Models.Base.BaseAddress>(item);
		}

        public NXA.Repository.Models.Base.BaseAddress GetBaseAddress(ID itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Base.BaseAddress>(itemId.Guid);
		}

        public NXA.Repository.Models.Base.BaseAddress GetBaseAddress(string path)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Base.BaseAddress>(path);		
		}

        public IEnumerable<NXA.Repository.Models.Base.BaseAddress> GetBaseAddresssFromSite(SiteContext site = null)
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			// Format path to safe quering
            var contentStartPath = site.ContentStartPath.GetFormattedPath();

			// Execute query and get all possible options
			var query =
				$"fast:{contentStartPath}//*[@@templateid='{NXA.Repository.Models.Base.BaseAddress.TemplateId}']";
			var allItems = Sitecore.Context.ContentDatabase.SelectItems(query).Select(GetBaseAddress).ToList();
			return allItems;
		}

        public NXA.Repository.Models.Base.BaseAddress GetBaseAddressFromSite(SiteContext site = null, string itemPath = "")
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			if (!string.IsNullOrEmpty(itemPath)) {
				var contentStartPath = site.ContentStartPath + itemPath;
				var path = $"/{contentStartPath}{itemPath}".GetFormattedPath();
				return GetBaseAddress(path);
			}

			var allItems = GetBaseAddresssFromSite(site);
			return allItems.Any() ? allItems.First() : null;
		}
    }
}


