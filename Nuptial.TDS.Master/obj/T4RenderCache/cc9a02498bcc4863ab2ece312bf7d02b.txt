//Revision: 76685cdc-46a4-4200-80ae-c28ce051cd51



namespace NXA.Repository.Models.Data.CssClass
{
	
	/// <summary>
	/// CssClassFolder
	/// <para></para>
	/// <para>Path: /sitecore/templates/NXA/Data/Css Class/Css Class Folder</para>	
	/// <para>ID: cc9a0249-8bcc-4863-ab2e-ce312bf7d02b</para>	
	/// </summary>
    [SitecoreType(TemplateId = "cc9a0249-8bcc-4863-ab2e-ce312bf7d02b", AutoMap = true)]
	public partial class CssClassFolder : BaseModel
	{
		public static ID TemplateId = new ID("cc9a0249-8bcc-4863-ab2e-ce312bf7d02b"); 

	    public struct FieldNames {
	    }
	    public struct FieldIds {
	    }
	    public struct FieldSources {
	    }

	

	}
}

namespace NXA.Repository.Interfaces.Data.CssClass
{	
    public partial interface ICssClassFolderRepository
    {
        NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolder(Item item);
        NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolder(Guid itemId);
        NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolder(ID itemId);
        NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolder(string path);
        IEnumerable<NXA.Repository.Models.Data.CssClass.CssClassFolder> GetCssClassFoldersFromSite(SiteContext site = null);
		IEnumerable<NXA.Repository.Models.Data.CssClass.CssClassFolder> GetCssClassFoldersFromSite(Item contextItem);
        NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolderFromSite(SiteContext site = null, string itemPath = "");
        NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolderFromSite(Item contextItem, string itemPath = "");
		NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolderFromGlobalSettings();
    }
}

namespace NXA.Repository.Repositories.Data.CssClass
{
    public partial class CssClassFolderRepository : BaseRepository, NXA.Repository.Interfaces.Data.CssClass.ICssClassFolderRepository
    {
        public NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolder(Item item)
		{
			return SitecoreContext.Cast<NXA.Repository.Models.Data.CssClass.CssClassFolder>(item);
		}

        public NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolder(Guid itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.CssClass.CssClassFolder>(itemId);
		}

        public NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolder(ID itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.CssClass.CssClassFolder>(itemId.Guid);
		}

        public NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolder(string path)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.CssClass.CssClassFolder>(path);		
		}

        public IEnumerable<NXA.Repository.Models.Data.CssClass.CssClassFolder> GetCssClassFoldersFromSite(SiteContext site = null)
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			// Format path to safe quering
            var contentStartPath = site.ContentStartPath.GetFormattedPath();

			// Execute query and get all possible options
			var query =
				$"fast:{contentStartPath}//*[@@templateid='{NXA.Repository.Models.Data.CssClass.CssClassFolder.TemplateId}']";
			var allItems = Sitecore.Context.Database.SelectItems(query).Select(GetCssClassFolder).ToList();
			return allItems;
		}

        public IEnumerable<NXA.Repository.Models.Data.CssClass.CssClassFolder> GetCssClassFoldersFromSite(Item contextItem)
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetCssClassFoldersFromSite(site);
        }

        public NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolderFromSite(SiteContext site = null, string itemPath = "")
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			if (!string.IsNullOrEmpty(itemPath)) {
				var contentStartPath = site.ContentStartPath + itemPath;
				var path = $"/{contentStartPath}{itemPath}".GetFormattedPath();
				return GetCssClassFolder(path);
			}

			var allItems = GetCssClassFoldersFromSite(site);
			return allItems.Any() ? allItems.First() : null;
		}

        public NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolderFromSite(Item contextItem, string itemPath = "")
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetCssClassFolderFromSite(site, itemPath);
        }

        private readonly NXA.Repository.Multisite.Providers.IGlobalSettingsProvider _settingsProvider =
            DependencyResolver.Current.GetService<NXA.Repository.Multisite.Providers.IGlobalSettingsProvider>();
        private Item _globalCssClassFolder;
        public NXA.Repository.Models.Data.CssClass.CssClassFolder GetCssClassFolderFromGlobalSettings()
		{
            if (_globalCssClassFolder != null)
                return GetCssClassFolder(_globalCssClassFolder);

            var settingsRoot = _settingsProvider.GetSettingsRoot();
            var item = settingsRoot.Axes.GetDescendants().FirstOrDefault(p =>
                p.TemplateID == NXA.Repository.Models.Data.CssClass.CssClassFolder.TemplateId);
			_globalCssClassFolder = item;
            return item==null ? null : GetCssClassFolder(item);
		}
    }
}


