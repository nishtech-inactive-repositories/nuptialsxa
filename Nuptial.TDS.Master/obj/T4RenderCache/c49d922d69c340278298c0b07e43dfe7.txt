//Revision: 345656c2-0d24-4090-8cac-56a2ad14b848



namespace NXA.Repository.Models.Data.CssClass
{
	
	/// <summary>
	/// CssClass
	/// <para></para>
	/// <para>Path: /sitecore/templates/NXA/Data/Css Class/Css Class</para>	
	/// <para>ID: c49d922d-69c3-4027-8298-c0b07e43dfe7</para>	
	/// </summary>
    [SitecoreType(TemplateId = "c49d922d-69c3-4027-8298-c0b07e43dfe7", AutoMap = true)]
	public partial class CssClass : BaseModel
	{
		public static ID TemplateId = new ID("c49d922d-69c3-4027-8298-c0b07e43dfe7"); 

	    public struct FieldNames {
	        public const string CssClassField = "Css Class";
	    }
	    public struct FieldIds {
	        public static readonly ID CssClassField = new ID("f4f3cf95-ee5a-49d4-9c05-e4e49bd33b68");
	    }
	    public struct FieldSources {
	    }

		/// <summary>
		/// The Css Class field.
		/// <para></para>
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: f4f3cf95-ee5a-49d4-9c05-e4e49bd33b68</para>
		/// <para>Custom Data: </para>
		/// </summary>
		[global::System.CodeDom.Compiler.GeneratedCodeAttribute("TDS - GlassItem.tt", "1.0")]
		[SitecoreField("Css Class")]
		public virtual string CssClassField  { get; set; }
	

	}
}

namespace NXA.Repository.Interfaces.Data.CssClass
{	
    public partial interface ICssClassRepository
    {
        NXA.Repository.Models.Data.CssClass.CssClass GetCssClass(Item item);
        NXA.Repository.Models.Data.CssClass.CssClass GetCssClass(Guid itemId);
        NXA.Repository.Models.Data.CssClass.CssClass GetCssClass(ID itemId);
        NXA.Repository.Models.Data.CssClass.CssClass GetCssClass(string path);
        IEnumerable<NXA.Repository.Models.Data.CssClass.CssClass> GetCssClasssFromSite(SiteContext site = null);
		IEnumerable<NXA.Repository.Models.Data.CssClass.CssClass> GetCssClasssFromSite(Item contextItem);
        NXA.Repository.Models.Data.CssClass.CssClass GetCssClassFromSite(SiteContext site = null, string itemPath = "");
        NXA.Repository.Models.Data.CssClass.CssClass GetCssClassFromSite(Item contextItem, string itemPath = "");
		NXA.Repository.Models.Data.CssClass.CssClass GetCssClassFromGlobalSettings();
    }
}

namespace NXA.Repository.Repositories.Data.CssClass
{
    public partial class CssClassRepository : BaseRepository, NXA.Repository.Interfaces.Data.CssClass.ICssClassRepository
    {
        public NXA.Repository.Models.Data.CssClass.CssClass GetCssClass(Item item)
		{
			return SitecoreContext.Cast<NXA.Repository.Models.Data.CssClass.CssClass>(item);
		}

        public NXA.Repository.Models.Data.CssClass.CssClass GetCssClass(Guid itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.CssClass.CssClass>(itemId);
		}

        public NXA.Repository.Models.Data.CssClass.CssClass GetCssClass(ID itemId)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.CssClass.CssClass>(itemId.Guid);
		}

        public NXA.Repository.Models.Data.CssClass.CssClass GetCssClass(string path)
		{
            return SitecoreContext.GetItem<NXA.Repository.Models.Data.CssClass.CssClass>(path);		
		}

        public IEnumerable<NXA.Repository.Models.Data.CssClass.CssClass> GetCssClasssFromSite(SiteContext site = null)
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			// Format path to safe quering
            var contentStartPath = site.ContentStartPath.GetFormattedPath();

			// Execute query and get all possible options
			var query =
				$"fast:{contentStartPath}//*[@@templateid='{NXA.Repository.Models.Data.CssClass.CssClass.TemplateId}']";
			var allItems = Sitecore.Context.Database.SelectItems(query).Select(GetCssClass).ToList();
			return allItems;
		}

        public IEnumerable<NXA.Repository.Models.Data.CssClass.CssClass> GetCssClasssFromSite(Item contextItem)
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetCssClasssFromSite(site);
        }

        public NXA.Repository.Models.Data.CssClass.CssClass GetCssClassFromSite(SiteContext site = null, string itemPath = "")
		{
            if (site == null)
                site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(Sitecore.Context.Item);

			if (!string.IsNullOrEmpty(itemPath)) {
				var contentStartPath = site.ContentStartPath + itemPath;
				var path = $"/{contentStartPath}{itemPath}".GetFormattedPath();
				return GetCssClass(path);
			}

			var allItems = GetCssClasssFromSite(site);
			return allItems.Any() ? allItems.First() : null;
		}

        public NXA.Repository.Models.Data.CssClass.CssClass GetCssClassFromSite(Item contextItem, string itemPath = "")
        {
            var site = NXA.Repository.Multisite.SiteContext.Current.GetSiteContext(contextItem);
            return GetCssClassFromSite(site, itemPath);
        }

        private readonly NXA.Repository.Multisite.Providers.IGlobalSettingsProvider _settingsProvider =
            DependencyResolver.Current.GetService<NXA.Repository.Multisite.Providers.IGlobalSettingsProvider>();
        private Item _globalCssClass;
        public NXA.Repository.Models.Data.CssClass.CssClass GetCssClassFromGlobalSettings()
		{
            if (_globalCssClass != null)
                return GetCssClass(_globalCssClass);

            var settingsRoot = _settingsProvider.GetSettingsRoot();
            var item = settingsRoot.Axes.GetDescendants().FirstOrDefault(p =>
                p.TemplateID == NXA.Repository.Models.Data.CssClass.CssClass.TemplateId);
			_globalCssClass = item;
            return item==null ? null : GetCssClass(item);
		}
    }
}


