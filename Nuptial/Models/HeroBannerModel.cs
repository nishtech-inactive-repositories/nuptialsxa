﻿using System;
using Sitecore.XA.Foundation.Variants.Abstractions.Models;

namespace Nuptial.Models
{
    public class HeroBannerModel: VariantsRenderingModel
    {
        public DateTime WeddingDate { get; set; }
        public bool HasWeddingDate { get; set; }
    }
}