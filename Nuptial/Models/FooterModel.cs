﻿namespace Nuptial.Models
{
    public class FooterModel : Sitecore.XA.Foundation.Mvc.Models.RenderingModelBase
    {
        public Footer Footer { get; set; }
    }
}