﻿using System;
using Sitecore.Data.Fields;

namespace Nuptial.Models
{
    public partial class NuptialSettings
    {
        public DateTime? WeddingDateTime {
            get
            {
                var field = (DateField) InnerItem.Fields[FieldIds.WeddingDate];
                return field?.DateTime;
            }
        }
    }
}