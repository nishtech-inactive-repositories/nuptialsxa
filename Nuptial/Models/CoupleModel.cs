﻿namespace Nuptial.Models
{
    public class CoupleModel : Sitecore.XA.Foundation.Mvc.Models.RenderingModelBase
    {
        public string Title { get; set; }

        public NuptialSettings Settings { get; set; }
    }
}