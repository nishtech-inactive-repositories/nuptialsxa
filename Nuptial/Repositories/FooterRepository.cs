﻿using Nuptial.Models;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Nuptial.Repositories
{
    public class FooterRepository: ModelRepository, IFooterRepository
    {
        public override IRenderingModelBase GetModel()
        {           
            var model = new FooterModel();
            FillBaseProperties(model);

            if (model.DataSourceItem == null)
                return model;

            var footer = new Footer(model.DataSourceItem);
            model.Footer = footer;

            return model;
        }
    }
}