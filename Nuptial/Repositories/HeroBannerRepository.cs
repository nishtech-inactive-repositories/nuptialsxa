﻿using Nuptial.Models;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using Sitecore.XA.Foundation.RenderingVariants.Repositories;

namespace Nuptial.Repositories
{
    public class HeroBannerRepository : VariantsRepository, IHeroBannerRepository
    {
        protected readonly INuptialSettingsRepository NuptialSettingsRepository;

        public HeroBannerRepository(INuptialSettingsRepository nuptialSettingsRepository)
        {
            NuptialSettingsRepository = nuptialSettingsRepository;
        }

        public override IRenderingModelBase GetModel()
        {
            var model = new HeroBannerModel();
            FillBaseProperties(model);

            var settings = NuptialSettingsRepository.GetModel();
            if (settings?.WeddingDateTime != null)
            {
                model.HasWeddingDate = true;
                model.WeddingDate = settings.WeddingDateTime.Value;
            }
            else
            {
                model.HasWeddingDate = false;
            }
            return model;
        }
    }
}