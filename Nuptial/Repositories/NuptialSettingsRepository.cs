﻿using System.Linq;
using Nuptial.Models;

namespace Nuptial.Repositories
{
    public class NuptialSettingsRepository : INuptialSettingsRepository
    {
        public NuptialSettings GetModel()
        {
            var context = new Sitecore.XA.Foundation.Multisite.MultisiteContext();
            var settingsItem = context.GetSettingsItem(Sitecore.Context.Item);

            var nuptialSettingsItem =
                settingsItem?.Children.FirstOrDefault(p => p.TemplateID == NuptialSettings.TemplateID);
            if (nuptialSettingsItem == null)
                return null;

            var model = new NuptialSettings(nuptialSettingsItem);
            return model;
        }
    }
}