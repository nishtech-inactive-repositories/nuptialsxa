﻿using Nuptial.Models;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Nuptial.Repositories
{
    public interface INuptialSettingsRepository: IAbstractRepository<NuptialSettings>
    {        
    }
}