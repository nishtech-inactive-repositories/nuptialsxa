﻿using Nuptial.Models;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;

namespace Nuptial.Repositories
{
    public class CoupleRepository : ModelRepository, ICoupleRepository
    {
        protected readonly INuptialSettingsRepository NuptialSettingsRepository;

        public CoupleRepository(INuptialSettingsRepository nuptialSettingsRepository)
        {
            NuptialSettingsRepository = nuptialSettingsRepository;
        }

        public override IRenderingModelBase GetModel()
        {           
            var model = new CoupleModel();
            FillBaseProperties(model);

            if (model.DataSourceItem != null)
            {
                var couple = new Couple(model.DataSourceItem);
                model.Title = couple.Title;
            }
            model.Settings = NuptialSettingsRepository.GetModel();
            
            return model;
        }
    }
}