﻿using Microsoft.Extensions.DependencyInjection;
using Nuptial.Controllers;
using Nuptial.Repositories;
using Sitecore.DependencyInjection;

namespace Nuptial.RegisterDependencies
{
    public class RegisterDependencies: IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IHeroBannerRepository, HeroBannerRepository>();
            serviceCollection.AddTransient<INuptialSettingsRepository, NuptialSettingsRepository>();
            serviceCollection.AddTransient<ICoupleRepository, CoupleRepository>();
            serviceCollection.AddTransient<IFooterRepository, FooterRepository>();
            serviceCollection.AddTransient<FooterController>();
            serviceCollection.AddTransient<CoupleController>();
            serviceCollection.AddTransient<HeroBannerController>();
            serviceCollection.AddTransient<NakedPlainHtmlController>();
            serviceCollection.AddTransient<SiblingRowsSplitterController>();            
        }
    }
}