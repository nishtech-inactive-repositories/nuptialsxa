﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Sitecore.Data.Items;
using Sitecore.XA.Feature.PageStructure.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;
using Sitecore.XA.Foundation.SitecoreExtensions.Utils;

namespace Nuptial.Controllers
{
    public class SiblingRowsSplitterController : StandardController
    {
        private const string OuterIdFieldName = "OuterId";
        private const string OuterStylesFieldName = "OuterStyles";
        private const string ContainerStylesFieldName = "ContainerStyles";

        protected override object GetModel()
        {
            var repository = DependencyResolver.Current.GetService<ISplitterRepository>();

            var model = (Sitecore.XA.Feature.PageStructure.Models.SplitterRenderingModel) repository.GetModel();

            // Get CSS for OuterStyles and ContainerStyles
            var outerStyles = model.Rendering.Parameters.Contains(OuterStylesFieldName)
                ? model.Rendering.Parameters[OuterStylesFieldName]
                : "";
            var containerStyles = model.Rendering.Parameters.Contains(ContainerStylesFieldName)
                ? model.Rendering.Parameters[ContainerStylesFieldName]
                : "";
            var outerId = model.Rendering.Parameters.Contains(OuterIdFieldName)
                ? model.Rendering.Parameters[OuterIdFieldName]
                : "";

            // Outer ID = -3
            if (!string.IsNullOrEmpty(outerId))
                model.Styles.Add(-3, outerId);

            // Outer = -2
            var sourceItems = ItemUtils.Lookup(outerStyles, Sitecore.Context.Database);
            if (sourceItems != null && sourceItems.Any())
            {
                var stringList = new List<string>();
                stringList.AddRange(sourceItems.Select(classItem => classItem["Value"]));
                model.Styles.Add(-2, string.Join(" ", stringList));
            }

            // Container = -1
            sourceItems = ItemUtils.Lookup(containerStyles, Sitecore.Context.Database);
            if (sourceItems != null && sourceItems.Any())
            {
                var stringList = new List<string>();
                stringList.AddRange(sourceItems.Select(classItem => classItem["Value"]));
                model.Styles.Add(-1, string.Join(" ", stringList));
            }

            return model;
        }
    }
}