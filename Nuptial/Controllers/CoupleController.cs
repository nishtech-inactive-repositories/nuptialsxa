﻿using System.Web.Mvc;
using Nuptial.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Nuptial.Controllers
{
    public class CoupleController : StandardController
    {
        private readonly ICoupleRepository _repository;

        public CoupleController(ICoupleRepository repository)
        {
            _repository = repository;
        }

        // GET: Couple
        public ActionResult ReusableIndex()
        {
            var ret = Index();
            return ret;
        }

        protected override object GetModel()
        {
            var model = _repository.GetModel();
            return model;
        }
    }
}