﻿using System.Web.Mvc;
using Nuptial.Repositories;
using Sitecore.XA.Foundation.RenderingVariants.Controllers;

namespace Nuptial.Controllers
{
    public class HeroBannerController  : VariantsController
    {
        protected readonly IHeroBannerRepository HeroBannerRepository;

        public HeroBannerController(IHeroBannerRepository heroBannerRepository)
        {
            HeroBannerRepository = heroBannerRepository;
        }

        public ActionResult ReusableIndex()
        {
            var ret = Index();
            return ret;
        }

        protected override object GetModel()
        {
            var model = HeroBannerRepository.GetModel();
            return model;
        }
    }
}