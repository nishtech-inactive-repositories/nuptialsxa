﻿using System.Web.Mvc;
using Nuptial.Repositories;
using Sitecore.XA.Foundation.Mvc.Controllers;

namespace Nuptial.Controllers
{
    public class FooterController : StandardController
    {
        private readonly IFooterRepository _repository;

        public FooterController(IFooterRepository footerRepository)
        {
            _repository = footerRepository;
        }

        // GET: Footer
        public ActionResult ReusableIndex()
        {
            var ret = Index();
            return ret;
        }

        protected override object GetModel()
        {
            var model = _repository.GetModel();
            return model;
        }
    }
}