﻿using System.Web.Mvc;

namespace Nuptial.Controllers
{
    public class NakedPlainHtmlController : Sitecore.XA.Foundation.Mvc.Controllers.StandardController
    {
        public ActionResult ReusableIndex()
        {
            var indexViewName = GetIndexViewName();
            return PartialView(indexViewName, this.GetModel());
        }
    }
}